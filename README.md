Maths Maze

Principe du jeu :
Le joueur (case rouge) va se déplacer dans un labyrinthe qui est fermé au départ. Quelques clés de couleurs variées seront réparties aléatoirement dans le labyrinthe. Sur l’écran sera affiché un calcul ou une équation. Chaque clé correspond à un nombre et au moins une de ces clés est la réponse au calcul. Le but est de récupérer les clés dans l’ordre pour pouvoir débloquer la sortie (jaune flashy) afin que le joueur puisse finir le labyrinthe et passer au niveau suivant.
Pour augmenter le niveau de difficulté, on peut augmenter la taille du labyrinthe ou complexifier les calculs.


Jour 1 : Sprint 1 = premier MVP
• créer tout ce qui se réfère aux calculs à l’aide de dictionnaires (additions, soustractions, multiplications).

• on utilise pour l’instant une grille faite « à la main ».
- fonction qui gère les déplacements commandés par le joueur (le joueur doit « bloquer » contre un mur).
- fonction qui place au hasard les clés dans les emplacements libres du labyrinthe ainsi que le joueur.

• actualisation des cases 
- fonction qui permet de « récupérer » les clés (difficultés lorsque le joueur passe sur une clé sans vouloir la récupérer).
- fonction qui affiche « game over » lorsque la mauvaise clé est récupérée.
- fonction qui gère l’ensemble des clés présentes sur le plateau et qui permet de détecter lorsque toutes les clés ont été récupérées.
- fonction qui crée une porte de sortie au hasard sur un bord du labyrinthe.


Jour 2 : Sprint 2 = Obtenir un jeu avec une interface graphique et utilisateur

• création de labyrinthes de toute taille ( = création de niveaux)
- fonction qui génère un labyrinthe de la taille voulue

• interface graphique
- les murs sont de couleur noir, le joueur est en rouge, les clés ont une couleur appartenant à une bibliothèque de 14 couleurs et les cases libres sont blanches.
- affichage de la légende (les cases pouvant être petites lorsque le labyrinthe est grand, la valeur associée à chaque clé est marqué sur le côté en légende).
- affichage et fonctionnalité des différents boutons « play », « quit », « next ».


Jour 3 = Sptrint 3 = obtenir une version jouable et plus élaboré du jeu pour la soutenance de vendredi
Peaufinage de certains points
•	Echelonnage de la difficulté avec des labyrinthes de plus en plus grands.
•	Affichage de l’écran d’accueil.
•	Implémentation de nouveaux calculs (pgcd, équations, pourquoi pas déterminants de matrices, les possibilités sont presque infinies)
•	Amélioration graphique (affichage du niveau auquel on est, règles du jeu)
