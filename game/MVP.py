from random import *

plateau = [['M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M'], ['M', ' ', 'M', ' ', ' ', ' ', ' ', ' ', 'M', ' ', ' ', ' ', 'M', ' ', ' ', ' ', 'M'], ['M', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', ' ', 'M', ' ', 'M', ' ', 'M', ' ', 'M'], ['M', ' ', ' ', ' ', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', 'M', ' ', 'M', ' ', 'M'], ['M', 'M', 'M', 'M', 'M', ' ', 'M', ' ', ' ', ' ', ' ', ' ', 'M', ' ', 'M', ' ', 'M'], ['M', ' ', ' ', 'M', ' ', ' ', 'M', ' ', 'M', 'M', 'M', 'M', 'M', ' ', 'M', ' ', 'M'], ['M', 'M', ' ', 'M', ' ', 'M', 'M', ' ', 'M', ' ', ' ', ' ', ' ', ' ', 'M', ' ', 'M'], ['M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M', ' ', 'M', 'M', 'M', 'M', 'M', 'M', 'M'], ['M', 'M', 'M', 'M', 'M', ' ', 'M', 'M', 'M', ' ', 'M', ' ', ' ', ' ', ' ', ' ', 'M'], ['M', ' ', ' ', ' ', 'M', ' ', ' ', ' ', ' ', ' ', 'M', ' ', 'M', 'M', 'M', ' ', 'M'], ['M', ' ', 'M', ' ', 'M', ' ', 'M', 'M', 'M', ' ', 'M', ' ', 'M', ' ', ' ', ' ', 'M'], ['M', ' ', 'M', ' ', ' ', ' ', 'M', ' ', ' ', ' ', ' ', ' ', 'M', 'M', 'M', 'M', 'M'], ['M', ' ', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', ' ', 'M', 'M', ' ', ' ', ' ', 'M'], ['M', ' ', ' ', ' ', ' ', ' ', 'M', ' ', ' ', 'M', ' ', 'M', ' ', ' ', 'M', ' ', 'M'], ['M', ' ', 'M', 'M', 'M', ' ', 'M', ' ', 'M', 'M', ' ', 'M', 'M', 'M', 'M', ' ', 'M'], ['M', ' ', ' ', ' ', ' ', ' ', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M'], ['M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M']]


def creer_dictionnaire(size):
    dico = {}
    for i in range(size):
        n1 = randint(1,10)
        n2 = randint(1,10)
        operations = [" + "," * "," - "]
        operation = choice(operations)
        calcul = str(n1)+operation+str(n2)
        if operation == " + ":
            reponse = n1 + n2
        if operation == " * ":
            reponse = n1 * n2
        if operation == " - ":
            reponse = n1 - n2
        dico[calcul] = reponse
    return(dico)


def deplacement(command) :
    global position, grille, valeur_precedente

    ligne,colonne = position
    if command == "g" :
        if grille[ligne][colonne-1] != "M" :
            grille[ligne][colonne] = valeur_precedente
            valeur_precedente = grille[ligne][colonne-1]
            grille[ligne][colonne-1] = "J"
            position = (ligne, colonne-1)
    if command == "d" :
        if grille[ligne][colonne+1] != "M" :
            grille[ligne][colonne] = valeur_precedente
            valeur_precedente = grille[ligne][colonne+1]
            grille[ligne][colonne+1] = "J"
            position = (ligne, colonne+1)
    if command == "h" :
        if grille[ligne-1][colonne] != "M" :
            grille[ligne][colonne] = valeur_precedente
            valeur_precedente = grille[ligne-1][colonne]
            grille[ligne-1][colonne] = "J"
            position = (ligne-1, colonne)
    if command == "b" :
        if grille[ligne+1][colonne] != "M" :
            grille[ligne][colonne] = valeur_precedente
            valeur_precedente = grille[ligne+1][colonne]
            grille[ligne+1][colonne] = "J"
            position = (ligne+1, colonne)


def afficher_labyrinthe(labyrinthe):
    for i in range(len(labyrinthe)):
        s = ""
        for j in range(len(labyrinthe[i])):
            if labyrinthe[i][j] == "M":
                s += "O"
            elif labyrinthe[i][j] == " ":
                s += " "
            elif labyrinthe[i][j] == "J":
                s += "*"
            elif labyrinthe[i][j] == "P":
                s += " "
            else:
                s += str(labyrinthe[i][j])
        print(s)


def ouvrir_porte():
    global grille
    liste_portes = [(0,3),(6,16),(12,16),(16,5)]
    choice=randint(0,3)
    porte = liste_portes[choice]
    grille[porte[0]][porte[1]] = "P"
    return porte

def cle() :
    global grille, position, calcul, dictionnaire, valeur_precedente, position_porte

    x,y = position
    if valeur_precedente == dictionnaire[calcul] :
        dictionnaire.pop(calcul)
        if dictionnaire != {} :
            calcul = choice(list(dictionnaire.keys()))
        else:
            position_porte = ouvrir_porte()
        valeur_precedente = " "
    elif valeur_precedente == " " :
        pass
    else :
        print("Vous avez perdu")
        quit()


def espaces_libres(grille):
    cases = []
    for i in range(len(grille)):
        for j in range(len(grille[i])):
            if grille[i][j] == " ":
                cases.append((i,j))
    return(cases)


def positionner_clefs(grille):
    global dictionnaire
    clefs = list(dictionnaire.values())
    cases_libres = espaces_libres(grille)
    for e in clefs:
        position = cases_libres.pop(randint(0,len(cases_libres)-1))
        grille[position[0]][position[1]] = e
    return grille


def positionner_joueur(grille):
    cases_libres = espaces_libres(grille)
    position = cases_libres.pop(randint(0,len(cases_libres)-1))
    grille[position[0]][position[1]] = "J"
    position_joueur = position
    return grille, position_joueur


def grille_initiale() :
    grille = positionner_clefs(plateau)
    grille, position_joueur = positionner_joueur(grille)
    return(grille, position_joueur)



def play_labyrinth(n) :
    global position, grille, valeur_precedente, dictionnaire, calcul

    dictionnaire = creer_dictionnaire(n)
    calcul = choice(list(dictionnaire.keys()))

    grille, position = grille_initiale()
    valeur_precedente = ' '
    afficher_labyrinthe(grille)

    while dictionnaire != {} :
        print(calcul)
        command = input('votre commande : h (haut), b (bas), d (droite), g (gauche), c récupérer la clé')
        if command == "c" :
            cle()
        elif command in "gdhb" :
            deplacement(command)
        else :
            print("entrer une commande valide")
            continue
        afficher_labyrinthe(grille)

    position_porte = ouvrir_porte()
    afficher_labyrinthe(grille)

    while position != position_porte :
        command = input('votre commande : h (haut), b (bas), d (droite), g (gauche), c récupérer la clé')
        if command == "c" :
            cle()
        elif command in "gdhb" :
            deplacement(command)
        else :
            print("entrer une commande valide")
            continue
        afficher_labyrinthe(grille)
    print("Tu as gagné")

play_labyrinth(2)
