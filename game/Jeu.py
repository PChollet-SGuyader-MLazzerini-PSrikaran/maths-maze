from tkinter import *
from random import *
from PIL import ImageTk, Image #Installer le module Pillow sur pycharm


Niveaux = { "1" : [["+"],15,4], "2" : [["+","-"],19,5], "3" : [["*","+","-"],23,8], "4" : [["*"],27,10], "5" : [["x","*","+","-"],31,10], "6" : [["x"], 35, 10], "7" : [["pgcd", "x", "*"],39,10]}
# On crée ici des niveaux, qui s'enchainent pas "ordre de difficulté". L'ordre de difficulté est déterminé par le type de calcul, la taille de la grille
# et le nombre de clé [calcul,taille,nombre de clé]

COLORS={"M": "black"," ": "white", "J": "red", "P" : "yellow"}
# ce dictionnaire attribue les couleurs aux différents élément ( Mur "M", case libre " ", joueur "J", porte "Yellow)? Il est complété plus loin par les clés
# disposée sur le plateau

Liste_couleur = ["blue","green2","orange","purple","cyan2","deep pink","green4","grey40","yellow","saddle brown","light coral","royal blue","green yellow","orange red"]
# Cettte liste de couleur sera utilisé pour les clés ( 14 au maximum)


def walk(x,y,lab,visited): #fonction récursive qui avance d'une case dans la création du labyrinthe
    visited[y][x] = 1 #la case actuelle est marquée visitée
    directions = [(x - 2, y), (x, y + 2), (x + 2, y), (x, y - 2)] #les différentes directions possibles
    shuffle(directions) #organisation aléatoire de la liste des directions
    for(new_x, new_y) in directions:
        if visited[new_y][new_x]:continue #si la prochaine case est visitée on teste la direction suivante
        if new_x == x-2:
            lab[y][x-1] = " " #on 'creuse' deux trous dans le labyrinthe vers le haut
            lab[y][x-2] = " "
        if new_x == x+2:
            lab[y][x+1] = " " #on 'creuse' deux trous dans le labyrinthe vers le bas
            lab[y][x+2] = " "
        if new_y == y-2:
            lab[y-1][x] = " " #on 'creuse' deux trous dans le labyrinthe vers la gauche
            lab[y-2][x] = " "
        if new_y == y+2:
            lab[y+1][x] = " " #on 'creuse' deux trous dans le labyrinthe vers la droite
            lab[y+2][x] = " "
        walk(new_x,new_y,lab,visited) #récursivité : on recommence à la position suivante.
        # Si à un moment aucune direction ne fonctionne on remontera dans les itérations pour repartir d'une case possédant une direction libre


def create_lab(size): #génération d'un labyrinthe carré de côté 'size"
    visited = [] #grille des cases déjà visitées par l'algorithme
    lab = [] #grille du labyrinthe

    for i in range(size+2):
        lab.append([])
        visited.append([])
        for j in range(size+2):
            lab[i].append("M") #on remplit le labyrinthe de murs
            if i == 0 or i == size + 1 or j == 0 or j == size + 1:
                visited[i].append(1) #les côtés du labyrinthe sont considérés comme visités
            else:
                visited[i].append(0) #le reste du labyrinthe n'est pas visité

    walk(6,6,lab,visited) #on appelle la fonction récursive 'walk'

    lab2 = [] #lab a une épaisseur de murs extérieurs de trop donc on ne sélectionne que l'intérieur
    for i in range(1,len(lab) - 1):
        lab2.append([])
        for j in range(1, len(lab) - 1):
            lab2[i-1].append(lab[i][j])

    return lab2


def espaces_libres(grille):               # Cette fonction permet de récupérer l'ensemble des cases libres du plateau
    cases = []
    for i in range(len(grille)):
        for j in range(len(grille[i])):
            if grille[i][j] == " ":
                cases.append((i,j))
    return(cases)


def positionner_clefs(grille):                     # cette fonction permet de positionner les clés (réponses aux calculs) à partir du dictionnaire en cours
    global dictionnaire                             # d'utilisation
    clefs = list(dictionnaire.values())
    cases_libres = espaces_libres(grille)
    for e in clefs:
        position = cases_libres.pop(randint(0,len(cases_libres)-1))        # on s'assure ici que 2 clés ne puissent se retrouver au même endroit
        grille[position[0]][position[1]] = str(e)
    return grille


def positionner_joueur(grille):                    # Cette fonction place le joueur aléatoirement sur la grille
    cases_libres = espaces_libres(grille)
    position = cases_libres.pop(randint(0,len(cases_libres)-1))
    grille[position[0]][position[1]] = "J"
    position_joueur = position
    return grille, position_joueur


def grille_initiale(size) :                          # Cette fonction crée une grille aléatoirement puis place les clés et le joueur
    grille = create_lab(size)
    grille = positionner_clefs(grille)
    grille, position_joueur = positionner_joueur(grille)
    return(grille, position_joueur)


def ouvrir_porte():                                             # Cette fonction ouvre une porte sur le bord du circuit (En haut) de manière aléatoire une fois
    global grille, GRILLE                                       # que toutes les clés ont été récupérées
    while True:
        porte = (0,randint(0,len(grille)-1))                       # on choisit une case au hasard sur le bord en haut de la grille
        if grille[porte[0] + 1][porte[1]] != "M":                  # on vérifie que l'on peut ouvrir une porte à cet endroit
            grille[porte[0]][porte[1]] = "P"                       # on ouvre alors la porte en changeant la valeur de la case
            frame1 = Frame(GRILLE, bg="yellow", relief='raised', borderwidth=1, height=frame_size, width=frame_size)
            frame1.grid(row=porte[0], column=porte[1])
            break
    return(porte)


def cle_graphic() :                                                               #Cette fonction est appelé lorsque le joueur tente de récupérer une clé via
    global calcul, dictionnaire, valeur_precedente, position_porte, Info, perdu   # la touche Entrée
    if dictionnaire != {}:
        if valeur_precedente == str(dictionnaire[calcul]) :                       # Si la valeur qu'on essye de récupérer est la clé attendu
            dictionnaire.pop(calcul)                                              # le calcul précédent est supprimer du dictionnaire
            if dictionnaire != {} :                                               # si ce n'était pas la dernière clé
                calcul = choice(list(dictionnaire.keys()))                        # on ajoute un nouveau calcul
                Info.grid_slaves()[1].config(text = calcul)                       # et on l'affiche
            else :
                position_porte = ouvrir_porte()                                   # si c'était la dernière clé on ouvre la porte
                for e in Info.grid_slaves():
                    e.destroy()
                Label(Info, text = "PORTE OUVERTE", font = ("Helvetica", "20"), bg = "grey80").grid()    # On indique au joueur que la porte a été ouverte
            valeur_precedente = " "                                                                      # dans les deux on supprime la clé du plateau
        elif valeur_precedente in " P" :                                          # si le joueur tente de récupérer une clé sur une case vide ou la porte on
            pass                                                                  # ne fait rien
        else:
            for e in Info.grid_slaves():                                          # s'il s'agissait d'une autre clé
                e.destroy()
            Label(Info, text = "VOUS AVEZ PERDU...", font = ("Helvetica", 20), bg = "grey80").grid()    # Alors le joueur a perdu
            Label(Info, text = "CLIQUEZ SUR RELANCER", font = ("Helvetica", 18), bg = "grey80").grid()   # Et il peut alors recommencer le niveau
            perdu = True


def deplacement(command) :                                                                    #Cette fonction gère l'ensemble des actions sur le plateau
    global position, grille, valeur_precedente, GRILLE, Info, position_porte, Boutons, perdu
    if position != position_porte and not perdu:                                              # Si le joueur n'a pas atteint la sortie et qu'il n'a pas perdu
        code_touche=command.keycode                                                           # on receuille la commande du joueur
        ligne,colonne = position
        if code_touche == 37:                                    # 3è correspond à un mouvement vers la droite
            if grille[ligne][colonne-1] != "M" :                 # on vérife que le mouvement est possible
                grille[ligne][colonne] = valeur_precedente       # on réattribue à la case où se trouve le joueur la valeur présente avant que celui-ci ne passe dessus
                frame2 = Frame(GRILLE, bg=COLORS[valeur_precedente], relief='raised', borderwidth=1, height=frame_size, width=frame_size)
                frame2.grid(row=position[0], column=position[1])
                valeur_precedente = grille[ligne][colonne-1]       # on met à jours valeur précédente pour stocker la valeur de la case où va se déplacer le joueur
                grille[ligne][colonne-1] = "J"                     # on déplace le joueur
                frame1 = Frame(GRILLE, bg="red", relief='raised', borderwidth=1, height=frame_size, width=frame_size)
                frame1.grid(row=ligne, column=colonne-1)
                position = (ligne,colonne-1)                       # on met à jours la position du joueur
        elif code_touche == 39 :
            if grille[ligne][colonne+1] != "M" :
                grille[ligne][colonne] = valeur_precedente
                frame2 = Frame(GRILLE, bg=COLORS[valeur_precedente], relief='raised', borderwidth=1, height=frame_size, width=frame_size)
                frame2.grid(row=position[0], column=position[1])
                valeur_precedente = grille[ligne][colonne+1]
                grille[ligne][colonne+1] = "J"
                frame1 = Frame(GRILLE, bg="red", relief='raised', borderwidth=1, height=frame_size, width=frame_size)
                frame1.grid(row=ligne, column=colonne+1)
                position = (ligne,colonne+1)
        elif code_touche == 40:
            if grille[ligne+1][colonne] != "M" :
                grille[ligne][colonne] = valeur_precedente
                frame2 = Frame(GRILLE, bg=COLORS[valeur_precedente], relief='raised', borderwidth=1, height=frame_size, width=frame_size)
                frame2.grid(row=position[0], column=position[1])
                valeur_precedente = grille[ligne+1][colonne]
                grille[ligne+1][colonne] = "J"
                frame1 = Frame(GRILLE, bg="red", relief='raised', borderwidth=1, height=frame_size, width=frame_size)
                frame1.grid(row=ligne+1, column=colonne)
                position = (ligne+1,colonne)
        elif code_touche == 38:
            if grille[ligne-1][colonne] != "M" :
                grille[ligne][colonne] = valeur_precedente
                frame2 = Frame(GRILLE, bg=COLORS[valeur_precedente], relief='raised', borderwidth=1, height=frame_size, width=frame_size)
                frame2.grid(row=position[0], column=position[1])
                valeur_precedente = grille[ligne-1][colonne]
                grille[ligne-1][colonne] = "J"
                frame1 = Frame(GRILLE, bg="red", relief='raised', borderwidth=1, height=frame_size, width=frame_size)
                frame1.grid(row=ligne-1, column=colonne)
                position = (ligne-1,colonne)
        elif code_touche == 13:                              # 13 correspond à ENTREE, le joueur veut récupérer une clé
            cle_graphic()                                    # on lance donc la fonction dédiée définie précedemment
    elif not perdu :                                         # si le joeueur atteint la sortie et qu'il n'a pas perdu
        for e in Info.grid_slaves():
            e.destroy()
        Label(Info, text = "VOUS AVEZ GAGNE !!!", font = ("Helvetica", 20), bg = "grey80").grid()   # On signifie au joueur qu'il a gagné
        Label(Info, text = "CLIQUEZ SUR NEXT", font = ("Helvetica", 20), bg = "grey80").grid(row = 1)   # il peut alors passer au niveau suivant
        Button(Boutons, text = "NEXT", command = next_level, font = ("Helvetica", "19")).grid(row = 1, column = 1)


def pgcd(x,y):                #Il s'agit ici d'une fonction personnel du PGCD, utilisée pour créer les dictionnaires de calcul
    if x<y:
        return pgcd(y,x)
    a=x
    b=y
    while b:
        (a,b)=(b,a%b)
    return a


def creer_dictionnaire(size):       # Cette fonction permet de créer des dictionnaire { Calcul : réponse } en fonction de l'operation voulue
    global operations
    dico = {}
    for i in range(size):
        operation = choice(operations)
        if operation == "+":
            n1 = randint(1,10)
            n2 = randint(1,10)
            calcul = str(n1)+operation+str(n2)+" = ?"
            reponse = n1 + n2
            dico[calcul] = str(reponse)
        if operation == "*":
            n1 = randint(1,10)
            n2 = randint(1,10)                             # Dans les 3 premiers if on programme des additions, soustractions et multiplications en prenant des
            calcul = str(n1)+operation+str(n2)+" = ?"      # des nombres au hasard
            reponse = n1 * n2
            dico[calcul] = str(reponse)
        if operation == "-":
            n1 = randint(1,10)
            n2 = randint(1,10)
            calcul = str(n1)+operation+str(n2)+" = ?"
            reponse = n1 - n2
            dico[calcul] = str(reponse)
        if operation == "x":
            reponse = randint(1,10)
            a = choice([-8,-7,-6,-5,-4,-3,-2,2,3,4,5,6,7,8])
            b = randint(1,15)
            c = a*reponse + b
            equation = str(a)+"x + "+str(b)+" = "+str(c)        # On crée ici un dictionnaire où les calculs sont des équations du premier degré
            dico[equation]= str(reponse)
        if operation == "pgcd":
            x = randint(1,999)                                # De même on crée ici un dictionnaire ou les calculs dont des PGCD
            y = randint(1,999)
            calcul = "pgcd("+str(x)+","+str(y)+")"
            dico[calcul] = str(pgcd(x,y))
    return(dico)


def next_level():                                                          # Cette fonction permet la transition entre les niveaux du jeux
    global niveau, Boutons, operations, taille, nb_clefs,GRILLE,Info
    Boutons.grid_slaves()[0].destroy()
    niveau += 1                                            # on met à jours le niveau
    taille = Niveaux[str(niveau)][1]                          # on selectionne la nouvelle taille
    nb_clefs = Niveaux[str(niveau)][2]                          # le nouveau nombre de clé
    operations = Niveaux[str(niveau)][0]                           # et le nouveau type de calcul
    for e in GRILLE.grid_slaves():                               # on supprime la grille précédente
        e.destroy()
    for e in Info.grid_slaves():                                  # et la légend précédente
        e.destroy()
    Boutons.grid_slaves()[0].config(text = "Niveau : "+str(niveau))
    partie()


def partie() :                          # cette fonction gère successivement les niveaux
    global position, grille, valeur_precedente, dictionnaire, calcul, GRILLE, position_porte, Info, frame_size, nb_clefs, taille, Window,perdu, Boutons

    if GRILLE.pack_slaves() != []:
        GRILLE.pack_slaves()[0].destroy()

    for e in Info.grid_slaves():
        e.destroy()

    if len(Boutons.grid_slaves()) == 5:
        Boutons.grid_slaves()[0].destroy()

    if taille%2 == 0:              # Le générateur de labyrinthe ne fonctionne que pour des tailles impaires
        taille += 1

    if nb_clefs > len(Liste_couleur):            # on s'assure qu'il n'y a pas plus de 14 clé car on ne dispose pas de plus de couleur pour les représenter
        nb_clefs = len(Liste_couleur)               # de manière discernable
    dictionnaire = creer_dictionnaire(nb_clefs)
    for k in range(len(list(dictionnaire.values()))):
        COLORS[str(list(dictionnaire.values())[k])] = Liste_couleur[k]        #On met à jours le dictionnaire COLORS pour les nouvelles clés

    frame_size = min(Window.winfo_screenwidth(), Window.winfo_screenheight() - 100) / taille   # on adapte la taille de la fenêtre de jeux à l'écran du joueur

    perdu = False
    grille,position = grille_initiale(taille)        #on crée la nouvelle grille
    calcul = choice(list(dictionnaire.keys()))        # on choisit le premier calcul au hasard
    valeur_precedente = " "                           # le joueur a été placé sur une case libre au départ, on initialise donc valeur_precedente comme case vide
    position_porte = (0,0)

    Label(Info, text='CALCUL EN COURS :', font = ("Helvetica", "20"), bg = "grey80").grid(row=0,column=0)     # on affiche le calcul en cours
    Label(Info, text = calcul, font = ("Helvetica", "20"), bg = "grey80").grid(row=1,column=0)

    frame_couleurs = Frame(Info, width = Info["width"], height = 40 * len(list(dictionnaire.values())), bg = "grey80")
    frame_couleurs.grid(row = 2)

    k = 0
    for e in list(dictionnaire.values()):                  # on affiche ici la légende pour les clés
        frame = Frame(frame_couleurs, width = frame_couleurs["width"]/2, height = 40, bg = COLORS[e])
        frame.grid(row = k, column = 0)
        Label(frame_couleurs, text = e, font = ("Helvetica", "20"), bg = "grey80").grid(row = k, column = 1)
        k+=1

    for i in range(len(grille)):                       # on affiche enfin la grille initiale
        for j in range(len(grille)):
            frame = Frame(GRILLE, bg=COLORS[grille[i][j]], relief='raised', borderwidth=1, height=frame_size, width=frame_size)
            frame.grid(row=i, column=j)
    Window.bind('<KeyPress>', deplacement)
    Window.mainloop()


def rules():          # Cette fonction permet d'afficher les règles du jeux
    global Window
    regles = Toplevel(Window)
    Label(regles, text = "Le but du jeu est de sortir du labyrinthe.\nPour cela il faut récupérer toutes les clefs (carrés de couleur), qui sont associées à des nombres. \nCependant il faut récupérer les clefs dans le bon ordre. \nLa bonne clef à récuperer est celle qui est le résultat du calcul affiché à l'écran.\nUne fois toutes les clefs récupérées, une porte s'ouvrira.\nVous controllez le carré rouge.\nLe jeu se joue avec les flèches directionnelles, appuyez sur entrée pour ramasser les clefs", font = ("Helvetica", "15")).pack()
    return


def premiere_partie():
    global Boutons, GRILLE, Info
    GRILLE.config(width = Window.winfo_screenheight() - 90, height = Window.winfo_screenheight() - 90) #On ajuste la taille de la fenetre
    Info.config(width = 300, height = GRILLE['height'] - 150)
    Boutons.grid_slaves()[3].config(text = "RELANCER") #Le bouton 'Jouer' devient le bouton relancer
    Boutons.grid_slaves()[3].config(command = partie)
    partie() #on lance une partie classique


def play():
    global Info, GRILLE, nb_clefs, taille, Window, Boutons, niveau, operations

    niveau  = 1 #début au niveau 1
    taille = Niveaux[str(niveau)][1]
    nb_clefs = Niveaux[str(niveau)][2]
    operations = Niveaux[str(niveau)][0]

    Window = Tk() #création de la fenêtre de jeu
    GRILLE = Frame(Window,height = 500, width = 500) #Composée d'une frame grille à gauche
    GRILLE.grid(row=0, column = 0, rowspan = 2)
    Info = Frame(Window, width = 300,height = 350, bg = "grey80") #Et d'une frame contenant les informations à droite
    Info.grid(row = 0, column = 1)
    Info.grid_propagate(0)
    Boutons = Frame(Window, height = 150, width = 300) #Ainsi qu'une frame contenant les boutons
    Boutons.grid(row = 1, column = 1)
    Boutons.grid_propagate(0)

    Button(Boutons, text = "JOUER", command = premiere_partie, font = ("Helvetica", "19")).grid(row = 0, column = 0) #Création des 4 boutons
    Button(Boutons, text = "QUITTER", command = quit,  font = ("Helvetica","19")).grid(row = 0, column = 1)
    Button(Boutons, text = "REGLES", command = rules, font = ("Helvetica", "19")).grid(row = 1, column = 0)
    Label(Boutons, text = "Niveau :"+str(niveau), font = ("Helvetica", "15")).grid(row = 2, column = 0)

    image = Image.open(r"background.bmp") #Placement de l'image de fond dans grille pour l'écran d'accueil

    img = ImageTk.PhotoImage(image)

    panel = Label(GRILLE, image = img)
    panel.pack(side = "bottom", fill = "both", expand = "yes")


    Window.mainloop() #Lancement de la fenetre

play()
